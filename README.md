
# home_center

Zadaniem jest napisanie aplikacji współpracującej z centralami Home Center 2 (w wersji 4.xxx).

Aplikacja musi umożliwiać:

pobranie podstawowych informacji o centrali

numer seryjny

adres mac

numer wersji

oprogramowania pobranie konfiguracji domu

pobranie zdefiniowanych sekcji i zawartych w nich pokoi

pobranie urządzeń znajdujących się w pokojach (wybrane typy: 'com.fibaro.binarySwitch', 'com.fibaro.multilevelSwitch')

umożliwienie sterowania urządzeniami

włączenie/wyłączenie modułów 'com.fibaro.binarySwitch'

włączenie/wyłączenie/ustawienie wartości modułów 'com.fibaro.multilevelSwitch'

odczyt i odświeżanie stanów urządzeń w czasie rzeczywistym.

Aplikacja musi być napisana w języku JavaScript (TypeScript) w najnowszych wersjach frameworku Angular