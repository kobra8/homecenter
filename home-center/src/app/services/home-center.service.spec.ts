import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { HomeCenterService } from './home-center.service';

describe('HomeCenterService', () => {
  let service: HomeCenterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HomeCenterService]
    });
    service = TestBed.inject(HomeCenterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
