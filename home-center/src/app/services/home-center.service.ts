import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SettingsInfo, Section, Room, Device, RefreshResponse } from '../model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiUrl } from '../api-url';

@Injectable({
  providedIn: 'root'
})
export class HomeCenterService {

  constructor(
    private _httpClient: HttpClient
  ) { }


  getSettingsInfo(): Observable<SettingsInfo> {
   return this._httpClient.get(ApiUrl.SETTINGS).pipe(map(response=> {
     return response as SettingsInfo
   }))
  }

  getSections(): Observable<Section[]> {
   return this._httpClient.get(ApiUrl.SECTIONS).pipe(map(response=> {
     return response as Section[]
   }))
  }

  getRooms(): Observable<Room[]> {
   return this._httpClient.get(ApiUrl.ROOMS).pipe(map(response=> {
     return response as Room[]
   }))
  }

  getDevices(): Observable<Device[]> {
   return this._httpClient.get(ApiUrl.DEVICES).pipe(map(response=> {
     return response as Device[]
   }))
  }

  setBinarySwitchStatus(deviceID, status) {
    return this._httpClient.get(ApiUrl.CALL_ACTION, {params:{deviceID, name: status}})
    .pipe(map(response=> {
      return response;
    }))
  }

  setMultilevelSwitchValue(deviceID, value) {
    return this._httpClient.get(ApiUrl.CALL_ACTION, 
      { params:{deviceID, name: 'setValue', arg1: value}})
    .pipe(map(response=> {
      return response;
    }))
  }

  refreshState(last = 0) {
    return this._httpClient.get(ApiUrl.REFRESH_STATES, 
      { params:{last}})
    .pipe(map(response=> {
      return response as RefreshResponse;
    }))
  }
}
