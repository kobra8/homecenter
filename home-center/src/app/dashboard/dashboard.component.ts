import { Component, OnInit } from '@angular/core';
import { HomeCenterService } from '../services/home-center.service';
import { SettingsInfo, Section, Room, Device } from '../model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  settingsInfo: SettingsInfo;
  sections: Section[];
  rooms: Room[];
  devices: Device[];

  constructor(
    private homeCenterService: HomeCenterService
  ) { }

  ngOnInit(): void {
    this.getSettingsInfo();
  }

  getSettingsInfo() {
    this.homeCenterService.getSettingsInfo().subscribe(settings => {
      this.settingsInfo = settings;
      this.getSections();
    })
  }
  getSections() {
    this.homeCenterService.getSections().subscribe(sections => {
      this.sections = sections;
      this.getRooms();
    })
  }
  getRooms() {
    this.homeCenterService.getRooms().subscribe(rooms => {
      this.rooms = rooms;
      this.getDevices();
    })
  }
  getDevices() {
    this.homeCenterService.getDevices().subscribe(devices => {
      devices.map(device => {
        if(device.type === 'com.fibaro.multilevelSwitch') {
          device.properties.value = Number(device.properties.value)
        }
      })
      let filteredByType = devices.filter(device => {
        return device.type === 'com.fibaro.binarySwitch' || device.type === 'com.fibaro.multilevelSwitch'
      })
      this.devices = filteredByType;
      this.mapDevices();
      this.mapRooms();
    })
  }

  mapRooms() {
    this.sections.map(section => {
      let filteredRooms = this.rooms.filter(room => {
        return room.sectionID === section.id
      })
      section.rooms = filteredRooms;
    })
  }

  mapDevices() {
    this.rooms.map(room => {
      let filteredDevices = this.devices.filter(device => {
        return device.roomID === room.id
      })
      room.devices = filteredDevices;
    })
  }

  toggleBinarySwitch(change, switchId) {
    const status = change.checked ? 'turnOn' : 'turnOff'
    this.homeCenterService.setBinarySwitchStatus(switchId, status).subscribe(res => {
      this.refreshState();
    })
  }

  setMultilevelSwitchValue(change, switchId) {
    const value = change.value;
    this.homeCenterService.setMultilevelSwitchValue(switchId, value).subscribe(res => {
      this.refreshState();
    })
  }

  refreshState() {
    this.getDevices();
    this.homeCenterService.refreshState().subscribe(res => {
      this.homeCenterService.refreshState(res.last).subscribe(res => {
        console.log('Refresh last:', res);
      })

    })
  }
}
