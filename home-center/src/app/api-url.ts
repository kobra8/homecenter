export class ApiUrl {

  private constructor() { }
  
  public static API_PREFIX = '/api';

  public static SETTINGS = ApiUrl.API_PREFIX + '/settings/info';

  public static SECTIONS = ApiUrl.API_PREFIX + '/sections';

  public static ROOMS = ApiUrl.API_PREFIX + '/rooms';

  public static DEVICES = ApiUrl.API_PREFIX + '/devices';

  public static CALL_ACTION = ApiUrl.API_PREFIX + '/callAction';

  public static REFRESH_STATES = ApiUrl.API_PREFIX + '/refreshStates';

  

  public static construct(url: string): string {
    return window.location.protocol + '//' + window.location.host + url;
  }

}