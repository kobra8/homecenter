
export interface SettingsInfo {
  serialNumber: string,
  mac: string,
  hcName: string,
  softVersion: string,
  beta: boolean,
  zwaveVersion: string,
  serverStatus: number,
  defaultLanguage: string,
  sunsetHour: string,
  sunriseHour: string,
  hotelMode: boolean,
  updateStableAvailable: boolean,
  updateBetaAvailable: boolean,
  batteryLowNotification: boolean
}

export interface Section {
  id: number,
  name: string,
  sortOrder: string,
  rooms: Room[]
}

export interface Room {
  id: number,
  name: string,
  sectionID: number,
  sortOrder: string,
  devices: Device[]
}

export interface Device {
  id: number,
  name: string,
  roomID: number,
  type: string,
  properties: {
    dead: number,
    disabled: number,
    value: number | string
  }
  actions: {
    setValue: number,
    turnOff: number,
    turnOn: number
  },
  sortOrder: string
}

export interface User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  authdata?: string;
}

export interface RefreshResponse {
  date: string;
  last: number;
  logs: any;
  status: string;
}